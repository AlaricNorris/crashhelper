package com.nrs.utils.tools ;

import java.io.File ;
import java.io.FileNotFoundException ;
import java.io.FileOutputStream ;
import java.io.FilenameFilter ;
import java.io.IOException ;
import java.io.PrintWriter ;
import java.io.StringWriter ;
import java.io.Writer ;
import java.lang.Thread.UncaughtExceptionHandler ;
import java.lang.reflect.Field ;
import java.text.SimpleDateFormat ;
import java.util.Arrays ;
import java.util.Date ;
import java.util.HashMap ;
import java.util.Map ;
import java.util.Properties ;
import java.util.TreeSet ;
import java.util.Vector ;
import android.content.Context ;
import android.content.pm.PackageInfo ;
import android.content.pm.PackageManager ;
import android.content.pm.PackageManager.NameNotFoundException ;
import android.os.Build ;
import android.os.Environment ;
import android.os.Looper ;
import android.util.Log ;
import android.widget.Toast ;

/**
 *	ClassName:	CrashHandler
 *	Function: 	UncaughtException处理类,当程序发生Uncaught异常的时候,由该类来接管程序,并记录发送错误报告.
 *							线程未捕获异常控制器是用来处理未捕获异常的。  
 *                          如果程序出现了未捕获异常默认情况下则会出现强行关闭对话框 
 *                          实现该接口并注册为程序中的默认未捕获异常处理  
 *                          这样当未捕获异常发生时，就可以做些异常处理操作 
 *                          例如：收集异常信息，发送错误报告 等。 
 *	@author   	Norris		Norris.sly@gmail.com
 *	@version  	
 *	@since   	Ver 1.0		I used to be a programmer like you, then I took an arrow in the knee 
 *	@Date	 	2013		2013-3-24		下午12:27:10
 *	@see 	 	
 *	──────────────────────────────────────────────────────────────────────────────────────────────────────
 *	@Fields 
 *	──────────────────────────────────────────────────────────────────────────────────────────────────────
 *	@Methods 
 *	──────────────────────────────────────────────────────────────────────────────────────────────────────
 *	2013-3-24下午12:27:10	Modified By Norris 
 *	──────────────────────────────────────────────────────────────────────────────────────────────────────
 */
public class CrashHandler implements UncaughtExceptionHandler {

	/**
	 * 	Log日志的tag
	 * 	String			:		TAG	
	 * 	@since 2013-3-21下午8:44:28
	 */
	private static final String TAG = "tag" ;

	/**
	 * 	系统默认的UncaughtException处理类
	 * 	Thread.UncaughtExceptionHandler			:		mDefaultHandler	
	 * 	@since 2013-3-21下午8:44:43
	 */
	private Thread.UncaughtExceptionHandler mDefaultHandler ;

	/**
	 * 	使用Properties来保存设备的信息和错误堆栈信息
	 * 	Properties			:		mDeviceCrashInfo	
	 * 	@since Ver 1.0
	 */
	private Properties mDeviceCrashInfo = new Properties() ;

	/**
	 * 	TODO
	 * 	String			:		VERSION_NAME	
	 * 	@since Ver 1.0
	 */
	private static final String VERSION_NAME = "versionName" ;

	/**
	 * 	TODO
	 * 	String			:		VERSION_CODE	
	 * 	@since Ver 1.0
	 */
	private static final String VERSION_CODE = "versionCode" ;

	/**
	 * 	TODO
	 * 	String			:		STACK_TRACE	
	 * 	@since Ver 1.0
	 */
	private static final String STACK_TRACE = "STACK_TRACE" ;

	/**
	 * 	错误报告文件的扩展名
	 * 	String			:		CRASH_REPORTER_EXTENSION	
	 * 	@since Ver 1.0
	 */
	private static final String CRASH_REPORTER_EXTENSION = ".cr" ;

	/**
	 * 	CrashHandler实例
	 * 	CrashHandler			:		mInstance	
	 * 	@since 2013-3-21下午8:44:53
	 */
	private static CrashHandler mInstance ;

	/**
	 * 	程序的Context对象
	 * 	Context			:		mContext	
	 * 	@since 2013-3-21下午8:45:02
	 */
	private Context mContext ;

	/**
	 * 	用来存储设备信息和异常信息
	 * 	Map<String,String>			:		mLogInfo	
	 * 	@since 2013-3-21下午8:46:15
	 */
	private Map<String , String> mDeviceInfo = new HashMap<String , String>() ;

	/**
	 * 	用于格式化日期,作为日志文件名的一部分(FIXME 注意在windows下文件名无法使用：等符号！)
	 * 	SimpleDateFormat			:		mSimpleDateFormat	
	 * 	@since 2013-3-21下午8:46:39
	 */
	private SimpleDateFormat mSimpleDateFormat = new SimpleDateFormat("yyyyMMdd_HH-mm-ss") ;

	/**
	 *	ClassName:	OnCrashListener
	 *	Function: 	Crash监听器，检测到Crash后，以观察者模式推送消息
	 *	@author   	AlaricNorris		Norris.sly@gmail.com
	 *	@version  	CrashHandler
	 *	@since   	Ver 1.0		I used to be a programmer like you, then I took an arrow in the knee 
	 *	@Date	 	2014		2014年4月7日		下午12:28:30
	 *	@see 	 	
	 */
	public interface OnCrashListener {

		public void onCrash() ;
	}

	/**
	 * 	崩溃监听器
	 * 	Vector<OnCrashListener>			:		mCrashListeners	
	 * 	@since Ver 1.0
	 */
	private Vector<OnCrashListener> mCrashListeners = new Vector<CrashHandler.OnCrashListener>() ;

	/**
	 * 	addOnCrashListener:(添加崩溃监听器)
	 *  ──────────────────────────────────
	 * 	@param inOnCrashListener    
	 * 	@throws 
	 * 	@since  	I used to be a programmer like you, then I took an arrow in the knee　Ver 1.0
	 */
	public void addOnCrashListener(OnCrashListener inOnCrashListener) {
		mCrashListeners.add(inOnCrashListener) ;
	}

	/**
	 * 	保证只有一个CrashHandler实例
	 */
	private CrashHandler() {
	}

	/**
	 * 	getInstance:{获取CrashHandler实例 ,单例模式 }
	 *  ──────────────────────────────────
	 * 	@return 	CrashHandler   
	 * 	@throws 
	 * 	@since  	I used to be a programmer like you, then I took an arrow in the knee　Ver 1.0
	 *	──────────────────────────────────────────────────────────────────────────────────────────────────────
	 *	2013-3-21下午8:52:24	Modified By Norris 
	 *	──────────────────────────────────────────────────────────────────────────────────────────────────────
	 */
	public static CrashHandler getInstance() {
		if(mInstance == null)
			mInstance = new CrashHandler() ;
		return mInstance ;
	}

	/**
	 * 	init:{初始化}
	 *  ──────────────────────────────────
	 * 	@param 		paramContext
	 * 	@return 	void   
	 * 	@throws 
	 * 	@since  	I used to be a programmer like you, then I took an arrow in the knee　Ver 1.0
	 *	──────────────────────────────────────────────────────────────────────────────────────────────────────
	 *	2013-3-21下午8:52:45	Modified By Norris 
	 *	──────────────────────────────────────────────────────────────────────────────────────────────────────
	 */
	public void init(Context paramContext) {
		mContext = paramContext ;
		// 获取系统默认的UncaughtException处理器
		mDefaultHandler = Thread.getDefaultUncaughtExceptionHandler() ;
		// 设置该CrashHandler为程序的默认处理器
		Thread.setDefaultUncaughtExceptionHandler(this) ;
	}

	/**
	 * 	当UncaughtException发生时会转入该重写的方法来处理
	 * 	(non-Javadoc)
	 * 	@see java.lang.Thread.UncaughtExceptionHandler#uncaughtException(java.lang.Thread, java.lang.Throwable)
	 */
	public void uncaughtException(Thread inThread , Throwable inThrowable) {
		if(mCrashListeners != null) {
			for(OnCrashListener tempCrashListener : mCrashListeners) {
				tempCrashListener.onCrash() ;
			}
		}
		if( ! handleException(inThrowable) && mDefaultHandler != null) {
			// 如果自定义的没有处理则让系统默认的异常处理器来处理
			mDefaultHandler.uncaughtException(inThread , inThrowable) ;
		}
		else {
			try {
				// 如果处理了，让程序继续运行1秒再退出，保证文件保存并上传到服务器
				Thread.sleep(1000) ;
			}
			catch(InterruptedException e) {
				e.printStackTrace() ;
			}
			// 退出程序
			android.os.Process.killProcess(android.os.Process.myPid()) ;
			System.exit(0) ;
		}
	}

	/**
	 * 	handleException:{自定义错误处理,收集错误信息 发送错误报告等操作均在此完成.}
	 *  ──────────────────────────────────
	 * 	@param 		inThrowable
	 * 	@return 	true:如果处理了该异常信息;否则返回false.   
	 * 	@throws 
	 * 	@since  	I used to be a programmer like you, then I took an arrow in the knee　Ver 1.0
	 *	──────────────────────────────────────────────────────────────────────────────────────────────────────
	 *	2013-3-24下午12:28:53	Modified By Norris 
	 *	──────────────────────────────────────────────────────────────────────────────────────────────────────
	 */
	public boolean handleException(Throwable inThrowable) {
		if(inThrowable == null)
			return false ;
		new Thread() {

			public void run() {
				Log.i("tag" , "run") ;
				Looper.prepare() ;
				Toast.makeText(mContext , "很抱歉,程序出现异常,即将退出" , 0).show() ;
				Looper.loop() ;
			}
		}.start() ;
		// 获取设备参数信息
		getDeviceInfo(mContext) ;
		// 保存日志文件
		saveCrashLogToFile(inThrowable) ;
//		// 收集设备信息  
//		collectCrashDeviceInfo(mContext) ;
//		// 保存错误报告文件  
//		String crashFileName = saveCrashInfoToFile(inThrowable) ;
//		// 发送错误报告到服务器  
//		sendCrashReportsToServer(mContext) ;
		return true ;
	}

	/** 
	 * 把错误报告发送给服务器,包含新产生的和以前没发送的. 
	 *  
	 * @param inContext 
	 */
	private void sendCrashReportsToServer(Context inContext) {
		String[] mFiles = getCrashReportFiles(inContext) ;
		if(mFiles != null && mFiles.length > 0) {
			TreeSet<String> sortedFiles = new TreeSet<String>() ;
			sortedFiles.addAll(Arrays.asList(mFiles)) ;
			for(String fileName : sortedFiles) {
				File mFile = new File(inContext.getFilesDir() , fileName) ;
				if(postReport(mFile)) {
					mFile.delete() ;// 删除已发送的报告  					
				}
			}
		}
	}

	/** 
	 * 获取错误报告文件名 
	 *  
	 * @param inContext 
	 * @return 
	 */
	private String[] getCrashReportFiles(Context inContext) {
		File filesDir = inContext.getFilesDir() ;
		// 实现FilenameFilter接口的类实例可用于过滤器文件名  
		FilenameFilter filter = new FilenameFilter() {

			// accept(File dir, String name)  
			// 测试指定文件是否应该包含在某一文件列表中。  
			public boolean accept(File dir , String name) {
				return name.endsWith(CRASH_REPORTER_EXTENSION) ;
			}
		} ;
		// list(FilenameFilter filter)  
		// 返回一个字符串数组，这些字符串指定此抽象路径名表示的目录中满足指定过滤器的文件和目录  
		return filesDir.list(filter) ;
	}

	private boolean postReport(File file) {
		// TODO 使用HTTP Post 发送错误报告到服务器  
		// 这里不再详述,开发者可以根据OPhoneSDN上的其他网络操作  
		// 教程来提交错误报告  
		return false ;
	}

	/**
	 * 	sendPreviousReportsToServer:(在程序启动时候, 可以调用该函数来发送以前没有发送的报告)
	 *  ──────────────────────────────────
	 * 	@param inContext    
	 * 	@throws 
	 * 	@since  	I used to be a programmer like you, then I took an arrow in the knee　Ver 1.0
	 */
	public void sendPreviousReportsToServer(Context inContext) {
		sendCrashReportsToServer(inContext) ;
	}

	/** 
	 * 保存错误信息到文件中 
	 *  
	 * @param ex 
	 * @return 
	 */
	private String saveCrashInfoToFile(Throwable ex) {
		Writer info = new StringWriter() ;
		PrintWriter printWriter = new PrintWriter(info) ;
		// printStackTrace(PrintWriter s)  
		// 将此 throwable 及其追踪输出到指定的 PrintWriter  
		ex.printStackTrace(printWriter) ;
		// getCause() 返回此 throwable 的 cause；如果 cause 不存在或未知，则返回 null。  
		Throwable cause = ex.getCause() ;
		while(cause != null) {
			cause.printStackTrace(printWriter) ;
			cause = cause.getCause() ;
		}
		// toString() 以字符串的形式返回该缓冲区的当前值。  
		String result = info.toString() ;
		printWriter.close() ;
		mDeviceCrashInfo.put(STACK_TRACE , result) ;
		try {
			Log.i("tag" , "" + mDeviceCrashInfo.getProperty(STACK_TRACE)) ;
			long timestamp = System.currentTimeMillis() ;
			String fileName = "crash-" + timestamp + CRASH_REPORTER_EXTENSION ;
			// 保存文件  
			FileOutputStream trace = mContext.openFileOutput(fileName , Context.MODE_PRIVATE) ;
			mDeviceCrashInfo.store(trace , "") ;
			trace.flush() ;
			trace.close() ;
			return fileName ;
		}
		catch(Exception e) {
			Log.e(TAG , "an error occured while writing report file..." , e) ;
		}
		return null ;
	}

	/** 
	 * 收集程序崩溃的设备信息 
	 *  
	 * @param ctx 
	 */
	public void collectCrashDeviceInfo(Context ctx) {
		try {
			// Class for retrieving various kinds of information related to the  
			// application packages that are currently installed on the device.  
			// You can find this class through getPackageManager().  
			PackageManager pm = ctx.getPackageManager() ;
			// getPackageInfo(String packageName, int flags)  
			// Retrieve overall information about an application package that is installed on the system.  
			// public static final int GET_ACTIVITIES  
			// Since: API Level 1 PackageInfo flag: return information about activities in the package in activities.  
			PackageInfo pi = pm
					.getPackageInfo(ctx.getPackageName() , PackageManager.GET_ACTIVITIES) ;
			if(pi != null) {
				// public String versionName The version name of this package,  
				// as specified by the <manifest> tag's versionName attribute.  
				mDeviceCrashInfo.put(VERSION_NAME , pi.versionName == null ? "not set"
						: pi.versionName) ;
				// public int versionCode The version number of this package,   
				// as specified by the <manifest> tag's versionCode attribute.  
				mDeviceCrashInfo.put(VERSION_CODE , pi.versionCode) ;
			}
		}
		catch(NameNotFoundException e) {
			Log.e(TAG , "Error while collect package info" , e) ;
		}
		// 使用反射来收集设备信息.在Build类中包含各种设备信息,  
		// 例如: 系统版本号,设备生产商 等帮助调试程序的有用信息  
		// 返回 Field 对象的一个数组，这些对象反映此 Class 对象所表示的类或接口所声明的所有字段  
		Field[] fields = Build.class.getDeclaredFields() ;
		for(Field field : fields) {
			try {
				// setAccessible(boolean flag)  
				// 将此对象的 accessible 标志设置为指示的布尔值。  
				// 通过设置Accessible属性为true,才能对私有变量进行访问，不然会得到一个IllegalAccessException的异常  
				field.setAccessible(true) ;
				mDeviceCrashInfo.put(field.getName() , field.get(null)) ;
				Log.d(TAG , field.getName() + " : " + field.get(null)) ;
			}
			catch(Exception e) {
				Log.e(TAG , "Error while collect crash info" , e) ;
			}
		}
	}

	/**
	 * 	getDeviceInfo:{获取设备参数信息}
	 *  ──────────────────────────────────
	 * 	@param 		paramContext
	 * 	@throws 
	 * 	@since  	I used to be a programmer like you, then I took an arrow in the knee　Ver 1.0
	 *	──────────────────────────────────────────────────────────────────────────────────────────────────────
	 *	2013-3-24下午12:30:02	Modified By Norris 
	 *	──────────────────────────────────────────────────────────────────────────────────────────────────────
	 */
	public void getDeviceInfo(Context paramContext) {
		try {
			// 获得包管理器
			PackageManager mPackageManager = paramContext.getPackageManager() ;
			// 得到该应用的信息，即主Activity
			PackageInfo mPackageInfo = mPackageManager.getPackageInfo(
					paramContext.getPackageName() , PackageManager.GET_ACTIVITIES) ;
			if(mPackageInfo != null) {
				String versionName = mPackageInfo.versionName == null ? "null"
						: mPackageInfo.versionName ;
				String versionCode = mPackageInfo.versionCode + "" ;
				mDeviceInfo.put("versionName" , versionName) ;
				mDeviceInfo.put("versionCode" , versionCode) ;
			}
		}
		catch(NameNotFoundException e) {
			e.printStackTrace() ;
		}
		// 反射机制
		Field[] mFields = Build.class.getDeclaredFields() ;
		// 迭代Build的字段key-value  此处的信息主要是为了在服务器端手机各种版本手机报错的原因
		for(Field field : mFields) {
			try {
				field.setAccessible(true) ;
				mDeviceInfo.put(field.getName() , field.get("").toString()) ;
				Log.d(TAG , field.getName() + ":" + field.get("")) ;
			}
			catch(IllegalArgumentException e) {
				e.printStackTrace() ;
			}
			catch(IllegalAccessException e) {
				e.printStackTrace() ;
			}
		}
	}

	/**
	 * 	saveCrashLogToFile:{将崩溃的Log保存到本地}
	 * 	TODO 可拓展，将Log上传至指定服务器路径
	 *  ──────────────────────────────────
	 * 	@param 		inThrowable
	 * 	@return		FileName
	 * 	@throws 
	 * 	@since  	I used to be a programmer like you, then I took an arrow in the knee　Ver 1.0
	 *	──────────────────────────────────────────────────────────────────────────────────────────────────────
	 *	2013-3-24下午12:31:01	Modified By Norris 
	 *	──────────────────────────────────────────────────────────────────────────────────────────────────────
	 */
	private String saveCrashLogToFile(Throwable inThrowable) {
//		Writer mmWriter = new StringWriter() ;
//		PrintWriter mmPrintWriter = new PrintWriter(mmWriter) ;
//		// printStackTrace(PrintWriter s)  
//		// 将此 throwable 及其追踪输出到指定的 PrintWriter  
//		inThrowable.printStackTrace(mmPrintWriter) ;
//		// getCause() 返回此 throwable 的 cause；如果 cause 不存在或未知，则返回 null。  
//		Throwable cause = inThrowable.getCause() ;
//		while(cause != null) {
//			cause.printStackTrace(mmPrintWriter) ;
//			cause = cause.getCause() ;
//		}
//		// toString() 以字符串的形式返回该缓冲区的当前值。  
//		String result = mmWriter.toString() ;
//		mmPrintWriter.close() ;
//		mDeviceCrashInfo.put(STACK_TRACE , result) ;
		StringBuffer mStringBuffer = new StringBuffer() ;
		for(Map.Entry<String , String> entry : mDeviceInfo.entrySet()) {
			String key = entry.getKey() ;
			String value = entry.getValue() ;
			mStringBuffer.append(key + "=" + value + "\r\n") ;
		}
		Writer mWriter = new StringWriter() ;
		PrintWriter mPrintWriter = new PrintWriter(mWriter) ;
		inThrowable.printStackTrace(mPrintWriter) ;
		inThrowable.printStackTrace() ;
		// getCause() 返回此 throwable 的 cause；如果 cause 不存在或未知，则返回 null。  
		Throwable mThrowable = inThrowable.getCause() ;
		// 迭代栈队列把所有的异常信息写入writer中
		while(mThrowable != null) {
			mThrowable.printStackTrace(mPrintWriter) ;
			// 换行  每个个异常栈之间换行
			mPrintWriter.append("\r\n") ;
			mThrowable = mThrowable.getCause() ;
		}
		//记得关闭
		mPrintWriter.close() ;
		// toString() 以字符串的形式返回该缓冲区的当前值。  
		String mResult = mWriter.toString() ;
		mStringBuffer.append(mResult) ;
		// 保存文件，设置文件名
		String mTime = mSimpleDateFormat.format(new Date()) ;
		String mFileName = "CrashLog-" + mTime + CRASH_REPORTER_EXTENSION ;
		if(Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
			try {
				File mDirectory = new File(Environment.getExternalStorageDirectory()
						+ "/CrashInfos") ;
				Log.i(TAG , mDirectory.toString()) ;
				if( ! mDirectory.exists())
					mDirectory.mkdir() ;
				FileOutputStream mFileOutputStream = new FileOutputStream(mDirectory + "/"
						+ mFileName) ;
				mFileOutputStream.write(mStringBuffer.toString().getBytes()) ;
				mFileOutputStream.close() ;
				return mFileName ;
			}
			catch(FileNotFoundException e) {
				e.printStackTrace() ;
			}
			catch(IOException e) {
				e.printStackTrace() ;
			}
		}
		return null ;
	}
}
