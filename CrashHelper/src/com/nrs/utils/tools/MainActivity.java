package com.nrs.utils.tools ;

import android.app.Activity ;
import android.content.Intent ;
import android.os.Bundle ;
import android.view.Menu ;
import com.nrs.utils.R ;

public class MainActivity extends Activity {

	private String s ;

	@ Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState) ;
		setContentView(R.layout.activity_main) ;
		startService(new Intent(getApplicationContext() , MyService.class)) ;
	}

	@ Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_main , menu) ;
		return true ;
	}
}
