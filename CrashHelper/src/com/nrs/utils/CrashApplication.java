package com.nrs.utils ;

import com.nrs.utils.tools.CrashHandler ;
import com.nrs.utils.tools.CrashHandler.OnCrashListener ;
import android.app.Application ;
import android.util.Log ;

public class CrashApplication extends Application implements OnCrashListener {

	@ Override
	public void onCreate() {
		super.onCreate() ;
		CrashHandler crashHandler = CrashHandler.getInstance() ;
		crashHandler.init(this) ;
		crashHandler.addOnCrashListener(this) ;
	}

	/**
	 * 	(non-Javadoc)
	 * 	@see com.nrs.utils.tools.CrashHandler.OnCrashListener#onCrash()
	 */
	public void onCrash() {
		// TODO Auto-generated method stub
		Log.i("tag" , "onCrash") ;
//		onTerminate() ;
	}
}
